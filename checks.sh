#!/usr/bin/env bash
#set -x
RED='\033[0;31m'
GREEN='\033[1;32m'
NC='\033[0m' # No Color

GULP_CHECK=$(curl --head --silent -S localhost:3000 2>&1)
GULP_STATUS=$?
GULP_MSG=$(echo "${GULP_CHECK}" | head -n 1)

if [ $GULP_STATUS -eq 0 ]; then
    echo -e "${GREEN}INFO:${NC} $GULP_MSG"
else
    echo -e "${RED}ERROR:${NC} $GULP_MSG"
fi

exit $GULP_STATUS
